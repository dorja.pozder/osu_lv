import pandas as pd
import matplotlib.pyplot as plt
data = pd. read_csv ('data_C02_emission.csv')

plt.figure()

#a
data['CO2 Emissions (g/km)'].plot(kind='hist',bins=20)

#b
fuel_colors = {'Z': 'blue', 'X': 'red', 'D': 'green', 'E': 'yellow', 'N': 'black'} 

for fuel_type, color in fuel_colors.items():
    df_fuel = data[data['Fuel Type'] == fuel_type]
    plt.scatter(df_fuel['Fuel Consumption City (L/100km)'], df_fuel['CO2 Emissions (g/km)'], c=color, label=fuel_type)
plt.show()

#c
data.boxplot(column =['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
#d
plt.figure()
data.groupby(by='Fuel Type').size().plot(kind='bar')
#e
data.groupby(by='Cylinders')['CO2 Emissions (g/km)'].mean().plot(kind='bar')
plt.show()


