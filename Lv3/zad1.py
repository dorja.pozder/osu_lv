import pandas as pd
data = pd. read_csv ('data_C02_emission.csv')

#a
print('A Zadatak:')

print('Dataframe sadrzi:',len(data),'mjerenja')
print('Tipovi podataka: ')
data.info()
print('Broj izostalih vrijednosti:',data.isnull().sum())
data = data.astype({"Make":'category',"Model":'category',"Vehicle Class":'category',"Transmission":'category',"Fuel Type":'category'}) 
data.info()

#b
print('B Zadatak:')

fuelSorted = data.sort_values(by = ['Fuel Consumption City (L/100km)'])
print('3 automobila s najvećom gradskom potrošnjom:\n',fuelSorted.head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print('3 automobila s najmanjom gradskom potrošnjom:\n',fuelSorted.tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#c
print('C Zadatak:')

engineSizeQuery = data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5) ]
print('Broj vozila s veličinom motora izmedju 2.5 i 3.5L :', len(engineSizeQuery))
print('Prosjecna CO2 emisija za filtrirana vozila je:', engineSizeQuery['CO2 Emissions (g/km)'].mean())

#d
print('D Zadatak:')

audiEntries = data[data['Make']=='Audi']
print('Broj mjerenja Audi automobila:', len(audiEntries))
audiEntries4C = audiEntries[audiEntries['Cylinders']==4]
print('Prosjecna CO2 emisija za Audi vozila je:', audiEntries4C['CO2 Emissions (g/km)'].mean())

#e
print('E Zadatak:')
cylinderQuery = data[(data['Cylinders']%2 == 0) & (data['Cylinders']>=4)]
print('Broj vozila s 4,6,8.. cilindara je:', len(cylinderQuery))
cylinderData = data.groupby('Cylinders')
print('CO2 emisija s obzirom na broj cilindara:\n', cylinderData[['CO2 Emissions (g/km)']].mean())

#f
print('F Zadatak:')

dieselEntries = data[data['Fuel Type']=='D']
gasolineEntries = data[data['Fuel Type']=='X']

print('Prosjecna gradska potrosnja za dizel vozila:', dieselEntries['Fuel Consumption City (L/100km)'].mean())
print('Median za dizel vozila:', dieselEntries['Fuel Consumption City (L/100km)'].median())
print('Prosjecna gradska potrosnja za benzin vozila:', gasolineEntries['Fuel Consumption City (L/100km)'].mean())
print('Median za benzin vozila:', gasolineEntries['Fuel Consumption City (L/100km)'].median())

#g
print('G Zadatak:')
filteredEntries = dieselEntries[dieselEntries['Cylinders']==4]
sortedEntries = filteredEntries.sort_values(by=['Fuel Consumption City (L/100km)'])
print('Dizel vozilo s 4 cilindra s najvecom gradskom potrosnjom goriva:', sortedEntries.head(1)[['Make','Model','Fuel Consumption City (L/100km)']] )

#h
print('H Zadatak:')
print(len(data[data['Transmission'].str.startswith('M')]))

#i
print(data.corr(numeric_only=True))