from sklearn import datasets
from sklearn . model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
import sklearn.linear_model as lm
import sklearn.metrics as skmetrics
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math


file = pd.read_csv('data_C02_emission.csv')
X_labels = ['Engine Size (L)',
            'Cylinders',
            'Fuel Consumption City (L/100km)',
            'Fuel Consumption Hwy (L/100km)',
            'Fuel Consumption Comb (L/100km)',
            'Fuel Consumption Comb (mpg)',
            ]
y_label = ['CO2 Emissions (g/km)']
X = file[X_labels].to_numpy()
y = file[y_label].to_numpy()
X_train , X_test , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state =1)


X_train_t = np.transpose(X_train)
X_test_t = np.transpose(X_test)
plt.scatter(x = X_train_t[2], y = y_train,s=20, c='#0000ff', label = 'Train')
plt.scatter(x = X_test_t[2], y = y_test, s=20, c='#ff0000', label = 'Test' )
plt.xlabel(X_labels[2])
plt.ylabel(y_label[0])
plt.legend()
plt.show()

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_train_scaled_t = np.transpose(X_train_scaled)
X_test_s = scaler.transform(X_test)

plt.hist(X_train_t[2])
plt.title('Before fit')
plt.show()

plt.hist(X_train_scaled_t[2])
plt.title('After fit')
plt.show()

linearModel = lm.LinearRegression()
linearModel.fit(X_train_scaled, y_train)
print(linearModel.coef_)

y_test_p = linearModel.predict(X_test_s)
plt.scatter(x = y_test, y = y_test_p, s = 20, c='#0000ff', label='Prediction')
plt.plot(y_test, y_test, color = 'black', linestyle = 'dashed', label='')
plt.xlabel('Test Values')
plt.ylabel('Predicted Values')
plt.show()

MSE = skmetrics.mean_squared_error(y_test, y_test_p)
print("MSE:", MSE)
print("RMSE:",math.sqrt(MSE))
print("MAE:",skmetrics.mean_absolute_error(y_test,y_test_p))
print("MAPE:",skmetrics.mean_absolute_percentage_error(y_test,y_test_p))
print("R Squared", skmetrics.r2_score(y_test,y_test_p))

